from setuptools import setup, find_packages

setup(
    name='ALA-Plateform-trainer',
    version='0.1',
    packages=find_packages(),
    install_requires=[
        'grpcio==1.48.2',
        'grpcio-tools==1.48.2',
        'torch==2.4.0',
        'datasets==2.21.0',
        'transformers[torch]==4.44.1',
        'numpy==2.1.0',
        'scikit-learn==1.5.1',
        'matplotlib==3.9.2'
    ],
)
