from enum import Enum


class JobType(Enum):
    SENTENCE_PARCING = 1
    ANNOTATION = 2
    TRAINING = 3

class Job():
    request_id: str
    job_id: str
    job_type: JobType
    job_data: {}