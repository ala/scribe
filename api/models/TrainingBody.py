from pydantic import BaseModel

class TrainingBody(BaseModel):
    server_url: str
    fondation_model_id: str
    finetuned_repo_name: str
    huggingface_token: str