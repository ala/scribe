from pydantic import BaseModel

class InfererConfig(BaseModel):
    provider: str
    model_id: str | None = None
    server_url: str | None = None
    openai_key: str | None = None