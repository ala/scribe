from setuptools import setup, find_packages

setup(
    name='ALA-Plateform-api',
    version='0.1',
    packages=find_packages(),
    install_requires=[
        'transformers==4.30.2',
        'protobuf==3.20.0',
        'spacy',
        'fr_dep_news_trf @ https://github.com/explosion/spacy-models/releases/download/fr_dep_news_trf-3.7.2/fr_dep_news_trf-3.7.2.tar.gz',
        'api==0.0.7',
        'benepar==0.2.0',
        'fastapi[standard]==0.112.1',
        'openai==1.42.0',
        'pydantic==2.8.2',
        'tinydb==4.8.0',
        'uvicorn==0.30.6',
        'neo4j==5.23.1',
        'grpcio==1.48.2',
        'grpcio-tools==1.48.2',
    ],
)
