import json
import grpc
from api.internal_services import neo4j
from api.internal_services.logger import logger
from api.protos.trainer import trainer_pb2_grpc, trainer_pb2


def find_all_occurrences(text, phrase):
    start = 0
    while True:
        start = text.find(phrase, start)
        if start == -1: return
        yield start
        start += len(phrase)


def start_training(job):
    training_data = []
    list_all_concept_ids = []
    training_sentences = neo4j.get_sentences_for_train()

    for tuple in training_sentences:
        sentence_id = tuple[0]
        concept_ids = tuple[1]
        sentence = neo4j.get_full_sentence_by_id(sentence_id)[0].strip()
        tags = []

        for concept_id in concept_ids:
            list_all_concept_ids.append(concept_id)
            span = neo4j.get_concept_text_by_id(concept_id)[0].strip()
            type = neo4j.get_concept_type_by_id(concept_id)[0]
            all_occurrences = list(find_all_occurrences(sentence, span))
            for start_index in all_occurrences:
                end_index = start_index + len(span)
                tags.append({
                    "start": start_index if start_index == 0 else start_index-1,
                    "end": end_index,
                    "tag": type,
                })

        training_data.append({
            'id': sentence_id,
            'text': sentence,
            'tags': tags
        })

    with grpc.insecure_channel(job.job_data['server_url']) as channel:
        stub = trainer_pb2_grpc.TrainerStub(channel)
        request = trainer_pb2.TrainingInput(
            training_data=json.dumps(training_data),
            fondation_model_id=job.job_data['fondation_model_id'],
            finetuned_repo_name=job.job_data['finetuned_repo_name'],
            huggingface_token=job.job_data['huggingface_token'],
        )
        response = stub.StartTraining(request)
        logger.debug(f"Incoming gRPC message : {response.status}")
    logger.debug(f"fin de la connexion gRPC")

    # passer toutes les origines des concepts en BERT
    neo4j.set_concept_after_training(list_all_concept_ids)
