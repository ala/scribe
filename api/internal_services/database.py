from tinydb import TinyDB, Query, where

db = TinyDB('db.json')

def get_last_sentence_index():
    result = db.search(where('key') == 'last_sentence_index')
    if not result:
        created_object = {'key': 'last_sentence_index', 'value': 0}
        db.insert(created_object)
        return 0
    else:
        return result[0]['value']

def update_last_sentence_index(value):
    db.update({'value': value}, where('key') == 'last_sentence_index')
    return value

def get_last_concept_index():
    result = db.search(where('key') == 'last_concept_index')
    if not result:
        created_object = {'key': 'last_concept_index', 'value': 0}
        db.insert(created_object)
        return 0
    else:
        return result[0]['value']

def update_last_concept_index(value):
    db.update({'value': value}, where('key') == 'last_concept_index')
    return value

def get_annotator_config():
    result = db.search(where('key') == 'annotator_config')
    if not result:
        created_object = {'key': 'annotator_config', 'value': { 'provider': 'NONE' }}
        db.insert(created_object)
        return created_object['value']
    else:
        return result[0]['value']

def update_annotator_config(value):
    get_annotator_config()
    db.update({'value': value}, where('key') == 'annotator_config')
    return value