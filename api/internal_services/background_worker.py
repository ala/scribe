from queue import Queue
from threading import Thread

import grpc

from api.internal_services.logger import logger
from api.internal_services.trainer import start_training
from api.models.Job import JobType
from api.protos.trainer import trainer_pb2_grpc, trainer_pb2

main_queue = Queue()
main_thread = None

trainer_queue = Queue()
trainer_thread = None


def process_main():
    global main_thread
    while main_queue.qsize() != 0:
        job = main_queue.get()
        logger.info(f"Processing the job {job.job_id}")
        if job is None:
            break

        match job.job_type:
            case JobType.SENTENCE_PARCING:
                from api.internal_services.spacy import parsing_and_load_in_neo4j
                parsing_and_load_in_neo4j(job)
            case JobType.ANNOTATION:
                from api.internal_services.annotator import annotation_process
                annotation_process(job)

        logger.info(f"Ending of the job {job.job_id}")
        main_queue.task_done()

    logger.info("Closing the worker thread")
    main_thread = None


def process_trainer():
    global trainer_thread
    while trainer_queue.qsize() != 0:
        job = trainer_queue.get()
        logger.info(f"Processing the job {job.job_id}")
        if job is None:
            break

        start_training(job)

        logger.info(f"Ending of the job {job.job_id}")
        trainer_queue.task_done()

    trainer_thread = None


def start_worker_thread(job_type: JobType):
    match job_type:
        case JobType.ANNOTATION | JobType.SENTENCE_PARCING:
            global main_thread
            if main_thread is None or not main_thread.is_alive():
                logger.info("Starting the main thread to process the queue")
                main_thread = Thread(target=process_main, daemon=True)
                main_thread.start()
        case JobType.TRAINING:
            global trainer_thread
            if trainer_thread is None or not trainer_thread.is_alive():
                logger.info("Starting the trainer thread to process the queue")
                trainer_thread = Thread(target=process_trainer, daemon=True)
                trainer_thread.start()


def add_job_to_queue(job):
    match job.job_type:
        case JobType.ANNOTATION | JobType.SENTENCE_PARCING:
            main_queue.put(job)
        case JobType.TRAINING:
            trainer_queue.put(job)

    start_worker_thread(job.job_type)
