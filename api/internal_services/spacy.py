import uuid

import benepar, spacy
import warnings

from api.internal_services.background_worker import add_job_to_queue
from api.models.Job import Job, JobType

warnings.filterwarnings("ignore")

from api.internal_services.database import get_last_sentence_index, update_last_sentence_index
from api.internal_services.neo4j import create_constituent_node, driver, create_constituent_relation, create_word_node, \
    create_next_word_relation, create_deprel_relation, create_sentence_node, create_sub_relation

benepar.download('benepar_fr2')
nlp = spacy.load('fr_dep_news_trf')
nlp.add_pipe('benepar', config={'model': 'benepar_fr2'})


def parsing_and_load_in_neo4j(job):
    sentence = job.job_data['sentence']
    last_index = update_last_sentence_index(get_last_sentence_index() + 1)

    with (driver.session() as session):
        doc = nlp(sentence)

        for i, sentence in enumerate(doc.sents):

            sentence_uuid = str(uuid.uuid4())

            #Création du noeud sentence
            session.execute_write(
                create_sentence_node,
                sentence_uuid
            )

            for constituent in sentence._.constituents:
                constituent_id = f"{last_index}.{i}.{constituent.start}-{constituent.end}"
                if constituent._.labels and constituent.root.text != constituent.text:
                    # Créer le consituant
                    session.execute_write(
                        create_constituent_node,
                        f"{last_index}.{i}.{constituent.start}-{constituent.end}",
                        constituent._.labels[0]
                    )

                    if constituent._.parent is not None:
                        # parent existe alors on crée le lien
                        session.execute_write(
                            create_constituent_relation,
                            f"{last_index}.{i}.{constituent._.parent.start}-{constituent._.parent.end}",
                            constituent_id
                        )

                else:
                    #Création du mot en noeud neo4j
                    session.execute_write(
                        create_word_node,
                        '.'.join(map(str, [last_index, i, constituent.root.i])),
                        constituent.text,
                        None if not hasattr(constituent, 'lemma_') else constituent.lemma_,
                        constituent.root.pos_,
                        True if constituent.root.dep_ == "root" else False
                    )

                    # Créer le mot et le constituant solitaire si nécessaire
                    if constituent._.labels:
                        # Créer le consituant
                        session.execute_write(
                            create_constituent_node,
                            f"{last_index}.{i}.{constituent.start}-{constituent.end}",
                            constituent._.labels[0]
                        )

                        session.execute_write(
                            create_constituent_relation,
                            f"{last_index}.{i}.{constituent.start}-{constituent.end}",
                            '.'.join(map(str, [last_index, i, constituent.root.i])),
                        )

                        session.execute_write(
                            create_constituent_relation,
                            f"{last_index}.{i}.{constituent._.parent.start}-{constituent._.parent.end}",
                            f"{last_index}.{i}.{constituent.start}-{constituent.end}",
                        )

                    else:
                        # parent existe alors on crée le lien
                        session.execute_write(
                            create_constituent_relation,
                            f"{last_index}.{i}.{constituent._.parent.start}-{constituent._.parent.end}",
                            '.'.join(map(str, [last_index, i, constituent.root.i])),
                        )

                    session.execute_write(
                        create_sub_relation,
                        '.'.join(map(str, [last_index, i, constituent.root.i])),
                        sentence_uuid,
                    )

            for token in sentence:
                #Création d'un lien de succession
                if token.i != 0:
                    idFrom = '.'.join(map(str, [last_index, i, token.i - 1]))
                    idTo = '.'.join(map(str, [last_index, i, token.i]))
                    session.execute_write(create_next_word_relation, idFrom, idTo)

                #dépendances syntaxiques
                idFrom = '.'.join(map(str, [last_index, i, token.head.i]))
                idTo = '.'.join(map(str, [last_index, i, token.i]))
                session.execute_write(create_deprel_relation, idFrom, idTo, token.dep_)

            new_job = Job()
            new_job.job_id = job.job_id
            new_job.job_type = JobType.ANNOTATION
            new_job.job_data = {'sentence': sentence, 'sentence_id': sentence_uuid}

            add_job_to_queue(new_job)

def simple_parsing(sentence):
    doc = nlp(sentence)
    output = []
    for i, sentence in enumerate(doc.sents):
        for token in sentence:
            output.append(token.text)
    return ' '.join(output)
