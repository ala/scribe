import os

from neo4j import GraphDatabase
from api.internal_services.logger import logger

uri = os.getenv('NEO4J_URL', "bolt://localhost:7687")
username = os.getenv('NEO4J_USERNAME', "neo4j")
password = os.getenv('NEO4J_PASSWORD', "password")

# Connexion à la base de données Neo4j
driver = GraphDatabase.driver(uri, auth=(username, password))


def create_word_node(tx, id, text, lemma, pos, root):
    tx.run('''
            CREATE (
                n:Word {
                    id: $id,
                    text: $text,
                    lemma: $lemma,
                    pos: $pos,
                    root: $root
                }
            )''',
           id=id, text=text, lemma=lemma, pos=pos, root=root)


def create_constituent_node(tx, id, type):
    tx.run('''
            CREATE (
                n:Constituent {
                    id: $id,
                    type: $type
                }
            )''',
           id=id, type=type)


def create_concept_node(tx, concept, id, used_for_training):
    tx.run('''
            CREATE (
                n:Concept {
                    type: $concept,
                    id: $id,
                    used_for_training: $used_for_training
                }
            )''',
           concept=concept, id=id, used_for_training=used_for_training)


def create_sentence_node(tx, id):
    tx.run('''
            CREATE (
                n:Sentence {
                    id: $id
                }
            )''',
           id=id)


def create_next_word_relation(tx, idFrom, idTo):
    tx.run('''
                MATCH
                (a:Word),
                (b:Word)
                WHERE a.id = $idFrom AND b.id = $idTo
                CREATE (a)-[r:NEXT]->(b)
           ''',
           idFrom=idFrom, idTo=idTo
           )


def create_deprel_relation(tx, idFrom, idTo, type):
    tx.run('''
                MATCH
                (a:Word),
                (b:Word)
                WHERE a.id = $idFrom AND b.id = $idTo
                CREATE (a)-[r:DEPREL {type: $type}]->(b)
           ''',
           idFrom=idFrom, idTo=idTo, type=type
           )


def create_concept_relation(tx, idFrom, idTo):
    tx.run('''
                MATCH
                (a:Concept),
                (b:Word)
                WHERE a.id = $idFrom AND b.id = $idTo
                CREATE (a)-[r:LINKED]->(b)
           ''',
           idFrom=idFrom, idTo=idTo
           )


def create_constituent_relation(tx, idFrom, idTo):
    tx.run('''
                MATCH
                (a:Constituent),
                (b:Word|Constituent)
                WHERE a.id = $idFrom AND b.id = $idTo
                CREATE (a)-[r:CONSREL]->(b)
           ''',
           idFrom=idFrom, idTo=idTo
           )


def create_sub_relation(tx, idFrom, idTo):
    tx.run('''
                MATCH
                (w:Word),
                (s:Sentence)
                WHERE w.id = $idFrom AND s.id = $idTo
                CREATE (w)-[r:SUB]->(s)
           ''',
           idFrom=idFrom, idTo=idTo
           )


def get_filtered_annotation(sentence_id, concept, annotation):
    annotation = annotation.strip().lower()

    if " " in annotation:
        from api.internal_services.spacy import simple_parsing
        annotation = simple_parsing(annotation)
        words_ids = get_id_multi_tokens(annotation, sentence_id)
    else:
        words_ids = get_id_single_tokens(annotation, sentence_id)

    if len(words_ids) == 0:
        logger.warn(
            f"Cannot find the following annotation '{annotation}' in the sentence id {sentence_id}. This error is a hallucination of large language models.")
        return set()

    filtered_annotation = set()
    if concept == "action":
        filtered_annotation = action(words_ids)
    elif concept == "actor":
        filtered_annotation = actor(words_ids)
    elif concept == "artifact":
        filtered_annotation = artifact(words_ids)
    elif concept == "condition":
        filtered_annotation = condition(words_ids, sentence_id)
    elif concept == "location":
        filtered_annotation = location(words_ids)
    elif concept == "modality":
        filtered_annotation = modality(words_ids)
    elif concept == "time":
        filtered_annotation = time(words_ids)
    elif concept == "reference":
        filtered_annotation = reference(words_ids)

    return filtered_annotation


def get_id_multi_tokens(annotation, sentence_id):
    annotation = annotation.split(" ")
    list_multi_token = driver.execute_query(
        '''
        WITH $array AS words
        MATCH path = (start:Word)-[:NEXT*]->(end:Word)
        MATCH (start)--(s:Sentence)
        where size(words) - 1 = size(relationships(path))
        and s.id = $sentence_id
        and all(
            idx IN range(0, size(words)-2)
            WHERE (toLower(words[idx]) = toLower((nodes(path)[idx]).text)
            AND toLower(words[idx+1]) = toLower((nodes(path)[idx + 1]).text))
        )
        and toLower(start.text) = words[0]
        and toLower(end.text) = words[size(words) - 1]
        with nodes(path) as result
        unwind result as results
        return collect(results.id) as liste
        ''',
        array=annotation,
        sentence_id=sentence_id
    )
    return list_multi_token.records[0][0]


def get_id_single_tokens(annotation, sentence_id):
    list_single_token = driver.execute_query(
        '''
        match (s:Sentence)--(w:Word)
        where toLower(w.text) = $annotation
        and s.id = $sentence_id
        with distinct w as results
        return collect(results.id) as liste 
        ''',
        annotation=annotation,
        sentence_id=sentence_id
    )
    return list_single_token.records[0][0]


def action(words_ids):
    nodes = driver.execute_query(
        '''
        match (c:Constituent)-[:CONSREL]->(w:Word)
        where c.type in ["VN", "VPinf", "VPpart"]
        and w.id in $array
        with c as constituent
        match (constituent)-[:CONSREL*..]->(w:Word)
        return distinct w.id
        ''',
        array=list(words_ids)
    )
    return set([record[0] for record in nodes.records])


def actor(words_ids):
    nodes = driver.execute_query(
        '''
        match (c:Constituent)-[:CONSREL]->(w:Word)
        where c.type in ["NP"]
        and w.id in $array
        with c as constituent
        match (constituent)-[:CONSREL*..]->(w:Word)
        return distinct w.id
        ''',
        array=list(words_ids)
    )
    return set([record[0] for record in nodes.records])


def artifact(words_ids):
    nodes = driver.execute_query(
        '''
        match (c:Constituent)-[:CONSREL]->(w:Word)
        where c.type in ["NP"]
        and w.id in $array
        with c as constituent
        match (constituent)-[:CONSREL*..]->(w:Word)
        return distinct w.id
        ''',
        array=list(words_ids)
    )
    return set([record[0] for record in nodes.records])


def condition(words_ids, sentence_id):
    sentence_id = str(sentence_id) + "."
    nodes = driver.execute_query(
        '''
        match (c:Constituent)-[:CONSREL*..]->(w:Word)
        where w.id in $array
        and c.type in ["Srel", "PP"]
        with c as constituent
        match (constituent)-[:CONSREL*..]->(w:Word)
        return distinct w.id
        
        UNION
        
        match (c:Constituent)-[:CONSREL]->(w:Word)
        where w.id in $array
        and c.type in ["Ssub"]
        with c as constituent
        match (constituent)-[:CONSREL]->(w:Word)
        return distinct w.id
        ''',
        array=list(words_ids),
        sentence_id=sentence_id
    )
    return set([record[0] for record in nodes.records])


def location(words_ids):
    nodes = driver.execute_query(
        '''
        match (c:Constituent)-[:CONSREL]->(w:Word)
        where c.type in ["NP"]
        and w.id in $array
        with c as constituent
        match (constituent)-[:CONSREL*..]->(w:Word)
        return distinct w.id
        ''',
        array=list(words_ids)
    )
    return set([record[0] for record in nodes.records])


def modality(words_ids):
    nodes = driver.execute_query(
        '''
        match (c:Constituent)-[:CONSREL]->(w:Word)
        where c.type in ["VN"]
        and w.id in $array
        with c as constituent
        match (constituent)-[:CONSREL*..]->(w:Word)
        return distinct w.id
        
        UNION
            
        match (c:Constituent)-[:CONSREL]->(w:Word)
        where c.type = "SENT"
        and w.id in $array
        return distinct w.id
        ''',
        array=list(words_ids)
    )
    return set([record[0] for record in nodes.records])


def time(words_ids):
    nodes = driver.execute_query(
        '''
        match (c:Constituent)-[:CONSREL]->(w:Word)
        where c.type = "NP"
        and w.id in $array
        with c as constituent
        match (constituent)-[:CONSREL*..]->(w:Word)
        return distinct w.id
        
        UNION
        
        match (c1:Constituent {type: "PP"})-[:CONSREL]->(c2:Constituent {type: "P+"})-[:CONSREL]->(w:Word)
        match (c2)<-[:CONSREL]-(:Constituent)-[:CONSREL]->(:Constituent {type: "NP"})
        where w.id in $array 
        with c1 as c1
        match (c1)-[:CONSREL*..]->(w:Word)
        return distinct w.id
        ''',
        array=list(words_ids)
    )
    return set([record[0] for record in nodes.records])


def reference(words_ids):
    nodes = driver.execute_query(
        '''
        match (c:Constituent)-[:CONSREL]->(w:Word)
        where c.type in ["NP", "PP"]
        and w.id in $array
        with c as constituent
        match (constituent)-[:CONSREL*..]->(w:Word)
        return distinct w.id
        ''',
        array=list(words_ids)
    )
    return set([record[0] for record in nodes.records])


def get_sentences_for_train():
    nodes = driver.execute_query(
        '''
        match (s:Sentence)--(:Word)--(c:Concept)
        where c.used_for_training = False
        return s.id as sentence, collect( distinct c.id) as concepts
        '''
    )
    return [(record[0], record[1]) for record in nodes.records]


def get_full_sentence_by_id(sentence_id):
    nodes = driver.execute_query(
        '''
        MATCH (s:Sentence{id:$sentence_id})--(w1:Word)
        MATCH (s:Sentence{id:$sentence_id})--(w2:Word)
        WHERE NOT EXISTS {
            MATCH (:Word)-[:NEXT]->(w1)
        }
        and not exists {
            match (w2)-[:NEXT]->(:Word)
        }
        WITH w1 AS startNode, w2 as endNode
        MATCH path = (startNode)-[:NEXT*]->(endNode)
        WITH nodes(path) AS nodelist
        WITH [node IN nodelist | node.text] AS texts
        WITH reduce(acc = "", t IN texts | acc + " " + t) AS concatenated_text
        RETURN concatenated_text
        LIMIT 1
        ''',
        sentence_id=sentence_id
    )
    return [record[0] for record in nodes.records]


def get_concept_text_by_id(concept_id):
    nodes = driver.execute_query('''
        match (c:Concept{id:$concept_id})--(w:Word)
        with collect(w) as word_list
        match (startNode:Word)
        match (endNode:Word)
        where startNode in word_list
        and endNode in word_list
        and not exists {
            match (w1:Word)-[:NEXT]->(startNode)
            where w1 in word_list
        }
        and not exists {
            match (endNode)-[:NEXT]->(w2:Word)
            where w2 in word_list
        }
        with startNode, endNode
        MATCH path = (startNode)-[:NEXT*]->(endNode)
        WITH nodes(path) AS nodelist
        WITH [node IN nodelist | node.text] AS texts
        WITH reduce(acc = "", t IN texts | acc + " " + t) AS concatenated_text
        RETURN concatenated_text
        LIMIT 1
        ''',
                                 concept_id=concept_id
                                 )
    return [record[0] for record in nodes.records]


def get_concept_type_by_id(concept_id):
    nodes = driver.execute_query('''
    match (c:Concept{id:$concept_id})
    return toLower(c.type)
    ''',
                                 concept_id=concept_id
                                 )
    return [record[0] for record in nodes.records]


def set_concept_after_training(concept_ids):
    nodes = driver.execute_query('''
        MATCH (c:Concept)
        WHERE c.id in $concept_ids
        SET c.used_for_training = True
        RETURN n
        ''', concept_ids=concept_ids)
