# logger_config.py
import logging

logger = logging.getLogger('uvicorn.error')
logger.setLevel(logging.DEBUG)