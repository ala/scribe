import json
from openai import OpenAI
from api.internal_services import database
from api.internal_services.logger import logger

def gpt_process(sentence):
    try:
        annotator_config = database.get_annotator_config()
        client = OpenAI(api_key=annotator_config['openai_key'])
        completion = client.chat.completions.create(
            model="gpt-4",
            messages=[
                {"role": "system", "content": get_pre_prompt()},
                {"role": "user", "content": sentence}
            ]
        )
        jsonOutput = json.loads(completion.choices[0].message.content)

        key_mappings = {
            "Action": "action",
            "Acteur": "actor",
            "Objet": "artifact",
            "Condition": "condition",
            "Lieu": "location",
            "Modalité": "modality",
            "Référence": "reference",
            "Temps": "time"
        }

        jsonOutput = {key_mappings.get(k, k): v for k, v in jsonOutput.items()}
        return jsonOutput

    except Exception as e:
        logger.error(f"Error during GPT process with the sentence : {sentence} | {e}")

def get_pre_prompt():
    return """
    Tu es un expert en NLP spécialisé dans l'extraction d'entités dans des phrases. 
    Ces entités comprennent l'Action, l'Acteur, l'Objet, la Condition, le Lieu, la Modalité, la Référence et le Temps. Ces concepts prennent les définitions suivantes : 
    * Action : le fait de faire quelque chose
    * Acteur : une entité qui a la capacité d'agir
    * Objet : élément physique fabriqué par l'homme et impliqué dans une action
    * Condition : une contrainte énonçant les propriétés qui doivent être respectées
    * Lieu : endroit où une action est effectuée
    * Modalité : un verbe indiquant la modalité de l'action (par exemple : peut, doit, etc)
    * Référence : mention d'autres dispositions légales ou textes juridiques affectant la disposition actuelle
    * Temps : le moment ou la durée associée à la réalisation d'une action
    Un élément de la phrase initiale peut posséder plusieurs classifications, tu dois extraire toutes les classifications possibles.
    Lors de l'analyse de textes, tes réponses doivent être formatées sous forme de JSON, listant les concepts identifiés sans élaboration ni justification.
    Le JSON sera de la forme suivante : 
    {
    "Action": [],
    "Acteur": [],
    "Objet": [],
    "Condition": [],
    "Lieu": [],
    "Modalité": [],
    "Référence": [],
    "Temps": []
    }
    Par exemple, avec la phrase suivante : "Le propriétaire ou détenteur d ' un véhicule routier qui trouve mal fondée une décision relative à la réception ou l ' immatriculation de son véhicule peut déférer celle-ci au ministre qui , après avoir demandé la position de la SNCA , confirme ou réforme celle-ci dans les deux mois à compter de l ' introduction du recours accompagné de toutes les pièces et informations utiles ."
    Vous devez obtenir : {"action": ["déférer celle-ci au ministre"], "actor": ["Le propriétaire ou détenteur d ' un véhicule routier", "ministre"], "condition": ["accompagné de toutes les pièces et informations utiles", "qui , après avoir demandé la position de la SNCA , confirme ou réforme celle-ci dans les deux mois à compter de l ' introduction du recours accompagné de toutes les pièces et informations utiles .", "qui trouve mal fondée une décision relative à la réception ou l ' immatriculation de son véhicule", "relative à la réception ou l ' immatriculation de son véhicule"], "modality": ["peut"], "time": ["après avoir demandé la position de la SNCA", "dans les deux mois à compter de l ' introduction du recours"]}
    Vous ne donnerez que le JSON comme réponse, aucune justification ou explication n'est accepté. De plus, vous ne devez pas reformuler les éléments extraits, ils doivent être identiques au mot près !
    """
