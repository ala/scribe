from fastapi import APIRouter
from api.internal_services import database
from api.internal_services.background_worker import add_job_to_queue
from api.internal_services.logger import logger
import uuid
from api.models.InfererConfig import InfererConfig
from api.models.Job import Job, JobType
from api.models.Sentence import Sentence
from api.models.TrainingBody import TrainingBody

router = APIRouter()


@router.post("/sentences")
def add_sentence_to_process(sentence: Sentence):
    logger.info(f"New sentence added to queue : {sentence.sentence}")
    new_job = Job()
    new_job.request_id = uuid.uuid4()
    new_job.job_id = uuid.uuid4()
    new_job.job_type = JobType.SENTENCE_PARCING
    new_job.job_data = {'sentence': sentence.sentence}
    add_job_to_queue(new_job)
    return {"message": "Job added to the queue for processing.", "job_id": new_job.job_id}


@router.post("/actions/training")
def add_sentence_to_process(training_body: TrainingBody):
    new_job = Job()
    new_job.request_id = uuid.uuid4()
    new_job.job_id = uuid.uuid4()
    new_job.job_type = JobType.TRAINING
    new_job.job_data = {
        "server_url": training_body.server_url,
        "fondation_model_id": training_body.fondation_model_id,
        "finetuned_repo_name": training_body.finetuned_repo_name,
        "huggingface_token": training_body.huggingface_token,
    }
    add_job_to_queue(new_job)
    return {"message": "Job added to the queue for processing.", "job_id": new_job.job_id}

@router.post("/actions/inferer/config")
def add_sentence_to_process(infererConfig: InfererConfig):
    new_config = {'provider': infererConfig.provider}

    match infererConfig.provider:
        case 'BERT':
            new_config['model_id'] = infererConfig.model_id
            new_config['server_url'] = infererConfig.server_url
        case 'GPT':
            new_config['openai_key'] = infererConfig.openai_key

    database.update_annotator_config(new_config)