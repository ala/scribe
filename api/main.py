# > fastapi dev main.py
# > uvicorn api.main:app --reload
import grpc
import uvicorn
from fastapi import FastAPI
from .routers import endpoints

app = FastAPI()

app.include_router(endpoints.router)

@app.get("/")
async def root():
    return {"message": "ALA plateform is running !"}


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)