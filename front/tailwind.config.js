/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./src/**/*.{js,jsx,ts,tsx}",
        "./node_modules/flowbite/**/*.js"
    ],
    theme: {
        extend: {
            colors: {
                primary: {
                    DEFAULT: '#0061FF',
                    50: '#B8D3FF',
                    100: '#A3C6FF',
                    200: '#7AADFF',
                    300: '#5294FF',
                    400: '#297AFF',
                    500: '#0061FF',
                    600: '#004CC7',
                    700: '#00368F',
                    800: '#002157',
                    900: '#000C1F',
                    950: '#000103'
                },
            },
        },
        fontFamily: {
            body: [
                'Inter',
                'ui-sans-serif',
                'system-ui',
                '-apple-system',
                'system-ui',
                'Segoe UI',
                'Roboto',
                'Helvetica Neue',
                'Arial',
                'Noto Sans',
                'sans-serif',
                'Apple Color Emoji',
                'Segoe UI Emoji',
                'Segoe UI Symbol',
                'Noto Color Emoji',
            ],
            sans: [
                'Inter',
                'ui-sans-serif',
                'system-ui',
                '-apple-system',
                'system-ui',
                'Segoe UI',
                'Roboto',
                'Helvetica Neue',
                'Arial',
                'Noto Sans',
                'sans-serif',
                'Apple Color Emoji',
                'Segoe UI Emoji',
                'Segoe UI Symbol',
                'Noto Color Emoji',
            ],
        },
        fontSize: {
            xs: ['0.65rem', { lineHeight: '1rem' }],
            sm: ['0.75rem', { lineHeight: '1.25rem' }],
            base: ['0.875rem', { lineHeight: '1.5rem' }],
            lg: ['1.9375rem', { lineHeight: '1.75rem' }],
            xl: ['1.125rem', { lineHeight: '1.75rem' }],
        },
    },
    plugins: [
        require('flowbite/plugin')
    ],
}

