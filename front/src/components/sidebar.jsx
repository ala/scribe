import SolarDocumentTextLinear from '~icons/solar/document-text-linear'
import SolarHelpOutline from '~icons/solar/help-outline'
import SolarHomeSmileBold from '~icons/solar/home-smile-bold'

export default function Sidebar() {

  return (
    <aside
      class="fixed top-0 left-0 z-40 w-72 h-screen pt-14 transition-transform -translate-x-full bg-white border-r border-gray-200 dark:bg-gray-800 dark:border-gray-700"
      aria-label="Sidenav"
      id="drawer-navigation"
    >
      {/* top section */}
      <div class="overflow-y-auto py-5 px-3 h-full bg-white dark:bg-gray-800">
        <ul class="space-y-2">
          <li>
            <a
              href="/"
              class="flex items-center p-2 text-base font-medium text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700 group"
            >
              <SolarHomeSmileBold />
              <span class="ml-3">My Home</span>
            </a>
          </li>
        </ul>
        <ul class="pt-5 mt-5 space-y-2 border-t border-gray-200 dark:border-gray-700">
          <li>
            <a
              href="https://docs.crackito.io"
              target="_blank"
              class="flex items-center p-2 text-base font-medium text-gray-900 rounded-lg transition duration-75 hover:bg-gray-100 dark:hover:bg-gray-700 dark:text-white group"
            >
              <SolarDocumentTextLinear />
              <span class="ml-3">Documentation</span>
            </a>
          </li>
          <li>
            <a
              href="https://matrix.to/#/#ala-plateform:matrix.org"
              target="_blank"
              class="flex items-center p-2 text-base font-medium text-gray-900 rounded-lg transition duration-75 hover:bg-gray-100 dark:hover:bg-gray-700 dark:text-white group"
            >
              <SolarHelpOutline />
              <span class="ml-3">Help</span>
            </a>
          </li>
        </ul>
      </div>

      {/* bottom section*/}
      <div class="hidden absolute bottom-0 left-0 justify-center p-4 space-x-4 w-full lg:flex bg-white dark:bg-gray-800 z-20">
      </div>
    </aside>
  )
}
